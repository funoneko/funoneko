# VOD Funo

## Title
Farming samples - HELLDIVERS 2 #1 [FunoNeko VOD - 2024-XX-XX]

## Description
Title: How 2 not dive...
Date: 2024-XX-XX
Activity: HELLDIVERS 2
Guests: @StreetDogs

🎬 Edited version: 

📄 Synopsis:
On affronte notre peur des espaces immenses grâce à Subnautica ! Si vous n'en avez jamais entendu parler c'est un jeu de survie/aventure/craft à l'ambiance très bien retranscrite et immersive. Dans un futur lointain où le voyage interstellaire existe, vous vous retrouvé rescapé du crash de votre vaisseau, alors en manoeuvre autour de la planète 4546B pour une mission concernant la corporation trans-gouvernementale Alterra. En plus de survivre sur cette planète étrangère recouverte d'eau, vous découvrirez bien vite que vous n'y êtes pas le premier...

🔗Links:
- Main Channel: 
- Twitch: 
- TikTok: 
- Twitter: 

🛍 Merchs:
- Cloth
- Cards
- ....

ℹ What is FunoNeko Live
This VOD channel gathers all FunoNeko streams, if you want to catch up on missed broadcasts or enjoy long-form content. Although my live streams are limited to 720p, this channel offers video in high-quality 2K (1440p).

🔍 Chapters:
00:00 - Start
01:00 - Gameplay
10:00 - End

Tags ?

#funo #funoneko

## TAGS (short tags = shadow ban ? / it seam big channel keep short tag) (https://www.reddit.com/r/letsplay/comments/4eapoe/youtube_tags_things_to_note/)

funoneko, funoneko vod, funoneko vods, funoneko game, funoneko games, funoneko twitch, funoneko youtube, funoneko stream, funoneko livestream, funoneko replay, funoneko archive, funoneko reddif, funoneko full livestream, funoneko gameplay, funoneko let's play, funoneko playthrough

funo, funo vod, funo vods, funo game, funo games, funo twitch, funo youtube, funo stream, funo livestream, funo replay, funo archive, funo reddif, funo full livestream, funo gameplay, funo let's play, funo playthrough,

funoneko helldivers vod, funo helldivers vod, hell divers, hell diver, helldivers, helldivers 2, helldivers II, helldivers vod, helldivers gameplay, helldivers playthrough, helldivers let's play, helldivers reaction, helldiver, helldiver 2, helldiver II, helldiver vod, helldiver gameplay, helldiver playthrough, helldiver let's play, helldiver reaction

--- Can be bad

twitch, vod, vods, stream, livestream, replay, live, reddif, archive, , games, game, gameplay, lets, let's, play, through, playthrough

## Tags channel

funo, neko, funovods, funovod

funoneko, funoneko vod, funoneko vods, funoneko stream, funoneko streamer, funoneko twitch, funoneko youtube, funoneko replay, funoneko rediff, funoneko live, funoneko livestream, funoneko let's play

funo, funo vod, funo vods, funo stream, funo streamer, funo twitch, funo youtube, funo replay, funo rediff, funo live, funo livestream, funo let's play

---

vod, vods, solo, let's play, game, video, fun