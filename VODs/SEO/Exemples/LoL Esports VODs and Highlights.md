# LoL Esports VODs and Highlights

## Vidéo regrouper ou séparer par activité ?
Regrouper / Séparer

## Titre
G2 v FNC | 2024 LEC Spring Finals | Week 6 Day 2 | G2 Esports vs. Fnatic | Game 4

## Description

VOD of G2 Esports vs. Fnatic




LEC Spring Split Finals 2024 #LEC




Casters: Dagda, Vedius, Drakos
Analyst: Sjokz, Guldborg, Finn, Laure, nuc


Full Line up:
G2 Esports Line up:
• Broken Blade – Top Zac
• Yike – Jungle Rek'Sai
• Caps – Mid Taliyah
• Hans Sama – Jinx
• Mikyx – Support Thresh


Fnatic Line up:
• Oscarinin – Top K'Sante
• Razork – Jungle Volibear
• Humanoid – Mid Azir
• Noah – ADC Varus
• Jun – Support Rakan


Watch all matches of the split here from all of our leagues: LCS, LEC, LCK, LPL. FULL VOD PLAYLIST -   

 / lolchampseries  

You can always learn more and view the full match schedule at http://www.lolesports.com.

Join the conversation on Twitter, Follow us @lolesports :
 

 / lolesports  

Like us on FACEBOOK for important updates:
 

 / lolesports  

Find us on INSTAGRAM:
 

 / lolesports  

Check out our photos on FLICKR:
https://flickr.com/photos/lecofficial

## Tags vidéo
/

## Tags channel
NALCS
NA
LCS
lolesports
league
of
legends
north
america