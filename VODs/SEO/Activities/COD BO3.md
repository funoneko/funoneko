## Title
 - COD BO3 campaign in French # [FunoNeko VOD - 2024-04-]

## Description
Activity: Call of Duty: Black Ops III

📄 Synopsis:
Drop in with me in in this futuristic world where I experiment for the first time the campaign of Call of Duty: Black Ops 3.
Black Ops III takes place in 2065, 40 years after the events of Black Ops II, amidst the transformative effects of climate change and the emergence of advanced technologies, including cybernetic enhancements.

## Tags

funo,neko,funoneko,funoneko vod,funoneko vods,funoneko game,funoneko stream,funoneko gameplay,funoneko let's play,funoneko playthrough,funoneko call of duty black ops 3, call of duty black ops 3, call of duty black ops 3 blind playthrough, call of duty black ops 3 gameplay, call of duty black ops 3 playtrough, call of duty black ops 3 reaction, call of duty black ops 3 let's play, call of duty black ops 3 vod, black ops, cod bo3