## Title
- HELLDIVERS 2 # [FunoNeko VOD - 2024-05-]

## Description
Activity: HELLDIVERS 2

📄 Synopsis:
Follow me on this adventure where I spread Managed Democracy across the galaxy in a very "proper" and "safe" manner*.
HELLDIVERS™ 2 is a 3rd person squad-based shooter that sees the elite forces of the Helldivers battling to win an intergalactic struggle to rid the galaxy of the rising alien threats.

## Tags
funo,neko,funoneko,funoneko vod,funoneko vods,funoneko game,funoneko stream,funoneko gameplay,funoneko let's play,funoneko playthrough,funoneko helldivers vod,funo helldivers vod,helldivers,helldivers 2,helldivers vod,helldivers gameplay,helldivers playthrough,helldivers let's play,helldivers reaction,helldiver,helldiver 2,helldiver vod,helldiver gameplay,helldiver playthrough,helldiver let's play,helldiver reaction,helldivers 2 gameplay