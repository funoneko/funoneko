## Description
Activity: Portal Reloaded

📄 Synopsis:
Portal Reloaded is a modified version of Portal 2, and it's my first time playing it.
The game's unique time-manipulation mechanics allow me to solve puzzles by moving through space and time. I can shoot a third "time portal" to traverse two different time periods in the puzzle chamber.

## Tags

funo,neko,funoneko,funoneko vod,funoneko vods,funoneko game,funoneko stream,funoneko gameplay,funoneko let's play,funoneko playthrough,funoneko portal,portal reloaded blind playthrough,portal reloaded gameplay,portal reloaded playtrough,portal reloaded reaction,portal mod,portal 2 mod,portal mods,portal 2 mods,portal,portal 2,portal reloaded