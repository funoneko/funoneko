## Title
 - COD MW 2019 campaign in French # [FunoNeko VOD - 2024-05-0]

## Description
Activity: Call of Duty: Modern Warfare

📄 Synopsis:
This is my first playthrough of Call of Duty: Modern Warfare 2019 campaign, I have put the language in French to let people discover what the French Prince sound like 😸.
Captain Price and the SAS partner with the CIA and the Urzikstani Liberation Force to retrieve stolen chemical weapons. The fight takes you from London to the Middle East and beyond, as this joint task force battles to stop a global war.

## Tags

funo,neko,funoneko,funoneko vod,funoneko vods,funoneko game,funoneko stream,funoneko gameplay,funoneko let's play,funoneko playthrough,funoneko call of duty modern warfare 2019, call of duty modern warfare 2019, call of duty modern warfare 2019 blind playthrough, call of duty modern warfare 2019 gameplay, call of duty modern warfare 2019 reaction, call of duty modern warfare 2019 let's play, call of duty modern warfare 2019 vod, modern warfare 2019, cod mw 2019